package com.sap.timp.base;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.prefs.Preferences;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TestBaseParalelo2{

	// TC2
	protected String tc2 = "http://as1-100-01-tc2:8000/timp/login/#/login";
	// TD1
	protected String td1 = "http://as1-100-01-td1:8000/timp/login/#/login";

	protected String tq1 = "http://as1-100-01-tq1:8000/timp/login/#/login";

	protected String tp1 = "http://as1-100-01-tp1:8000/timp/login/#/login";
	
	public ThreadLocal<ChromeDriver> driver2 = new ThreadLocal<ChromeDriver>();

	public WebDriver getDriver() {
		return driver2.get();
	}
	
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "./src/test/resources/chromedriver/chromedriverX86.exe");
		driver2.set(new ChromeDriver());
	}
	
	

	
	
	public void tearDown() {
		getDriver().quit();
	}
	/*
	public WebDriver initializationL() {
		
		

		
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get(td1);

		return driver;

	}
	*/
	
	
public void close() {

		
	}

	public void sleep(int miliSeconds) {
		try {
			Thread.sleep(miliSeconds);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public Boolean igualInt(int valor, int esperado) {
		
		boolean sucesso = false;
		if (valor == esperado) {
			sucesso = true;
		}else {
			sucesso = false;
		}
		
		return sucesso;
	}
	
	public Boolean igualDobule(double valor, double esperado) {
		
		boolean sucesso = false;
		if (valor == esperado) {
			sucesso = true;
		}else {
			sucesso = false;
		}
		
		return sucesso;
	}
	
	public boolean isNum(String strNum) {
	    boolean ret = true;
	    try {

	        Double.parseDouble(strNum);

	    }catch (NumberFormatException e) {
	        ret = false;
	    }
	    return ret;
	}
	
	
	
	public Boolean mayorQue(double mayor, double menor ) {
		
		boolean sucesso = false;
		if (mayor > menor) {
			sucesso = true;
		}else {
			sucesso = false;
		}
		
		return sucesso;
	}
	
	public Boolean menorQue(double mayor, double menor ) {
		
		boolean sucesso = false;
		if (menor < mayor) {
			sucesso = true;
		}else {
			sucesso = false;
		}
		
		return sucesso;
	}

	public void waitExpectXpath(String locator) {
		WebDriverWait wait = new WebDriverWait((WebDriver) driver2, 15000);

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));

	}
	
	public String remplazarPuntos(String valor) {
		
		valor = valor.replace(".", "");

		return valor;

	}
	
	public String remplazarComas(String valor) {
		
		valor = valor.replace(",", ".");
		
		return valor;
		
		
	}
	
	public String formatear(String valor) {
		
		valor = valor.replace(".", "");
		valor = valor.replace(",", ".");
		
		return valor;
	}

	public void waitExpectElement(WebElement element) {
		WebDriverWait wait = new WebDriverWait((WebDriver) driver2, 15000);

		wait.until(ExpectedConditions.elementToBeClickable(element));

	}

	public void actionsMoveToElementXpath(String xpath) {
		Actions actions = new Actions(getDriver());
		actions.moveToElement(((WebDriver) driver2).findElement(By.xpath(xpath))).perform();
	}

	public void actionsMoveToElementElement(WebElement element) {
		Actions actions = new Actions(getDriver());
		actions.moveToElement(element).perform();
	}

	public void invisibilityOfElement(String xpath) {
		WebDriverWait wait = new WebDriverWait(getDriver(), 300000);

		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(xpath)));

	}

	public void attributeToBeXpath(String locator, String attribute, String value) {
		WebDriverWait wait = new WebDriverWait(getDriver(), 15000);

		wait.until(ExpectedConditions.attributeToBe(By.xpath(locator), attribute, value));
	}

	public void attributeToBeElement(WebElement element, String attribute, String value) {
		WebDriverWait wait = new WebDriverWait(getDriver(), 15000);

		wait.until(ExpectedConditions.attributeToBe(element, attribute, value));
	}

	public void dobleClickXpath(String locator) {

		Actions actions = new Actions(getDriver());
		actions.doubleClick(((WebDriver) driver2).findElement(By.xpath(locator))).perform();

	}

	public void dobleClickElement(WebElement element) {

		Actions actions = new Actions((WebDriver) driver2);
		actions.doubleClick(element).perform();

	}

	public void moveToElement(WebElement element, WebElement hacia) {
		Actions actions = new Actions((WebDriver) driver2);

		actions.dragAndDrop(element, hacia).perform();

	}

	public String fechaActual() {

		Date fecha = new Date();

		SimpleDateFormat df = new SimpleDateFormat("dd/MM/YYYY");

		return df.format(fecha);

	}

	public String fechaAyer() {

		Date fecha = new Date();

		Date ayer = new Date(fecha.getTime() + TimeUnit.DAYS.toMillis(-1));

		SimpleDateFormat df = new SimpleDateFormat("dd/MM/YYYY");

		return df.format(ayer);

	}
	

	public String fechaManana() {

		Date fecha = new Date();

		Date ayer = new Date(fecha.getTime() + TimeUnit.DAYS.toMillis(+1));

		SimpleDateFormat df = new SimpleDateFormat("dd/MM/YYYY");

		return df.format(ayer);

	}

	public Double convertToDouble(String numero) {

		Double result = new Double(numero);

		return result;

	}
	
	public String getText(WebElement element) {

		String texto = element.getText();
		
		if (texto.isEmpty()==true) {
			texto = "vazio";
		}
		
		return texto;

	}
	
	public String textContent(WebElement element) {

		String texto = element.getAttribute("textContent");
		
		return texto;

	}
	
	public String getValue(WebElement element) {

		String texto = element.getAttribute("value");
		
		if (texto.isEmpty()==true) {
			texto = "vazio";
		}
		
		return texto;

	}
	
	
	public int convertToInt(String numero) {

		int result = new Integer(numero);

		return result;

	}

	public void attributoNotToBeEmptyElement(WebElement element, String attribute) {
		WebDriverWait wait = new WebDriverWait((WebDriver) driver2, 15000);

		wait.until(ExpectedConditions.attributeToBeNotEmpty(element, attribute));
	}

	public void attributoNotToBeEmptyXpath(String xpath, String attribute) {
		WebDriverWait wait = new WebDriverWait((WebDriver) driver2, 15000);

		wait.until(ExpectedConditions.attributeToBeNotEmpty(((WebDriver) driver2).findElement(By.xpath(xpath)), attribute));
	}

	public void visibilityOfElementXpath(String xpath) {

		boolean isPresent = ((WebDriver) driver2).findElement(By.xpath(xpath)).isDisplayed();

		while (isPresent == false) {
			sleep(3000);
			isPresent = ((WebDriver) driver2).findElement(By.xpath(xpath)).isDisplayed();
		}

	}

	public Integer contarWebElementsList(List<WebElement> colunas) {

		int contar = -1;

		if (colunas.size() > 0) {

			for (int i = 0; i < colunas.size(); i++) {
				contar = contar + 1;
			}
		} else {
			contar = 0;
		}

		return contar;

	}

	public void idInserir1(String idRegistro) {

		Preferences id = Preferences.userRoot();

		id.put("idR1", idRegistro);

	}

	public String idObter1() {

		Preferences id = Preferences.userRoot();

		long idRegistro = id.getLong("idR1", 1);

		String idReturn = String.valueOf(idRegistro);

		return idReturn;

	}

	public void idInserir2(String idRegistro) {

		Preferences id = Preferences.userRoot();

		id.put("idR2", idRegistro);

	}

	public String idObter2() {

		Preferences id = Preferences.userRoot();

		long idRegistro = id.getLong("idR2", 1);

		String idReturn = String.valueOf(idRegistro);

		return idReturn;

	}

	public void idInserir3(String idRegistro) {

		Preferences id = Preferences.userRoot();

		id.put("idR3", idRegistro);

	}

	public String idObter3() {

		Preferences id = Preferences.userRoot();

		long idRegistro = id.getLong("idR3", 1);

		String idReturn = String.valueOf(idRegistro);

		return idReturn;

	}

	public void idInserir4(String idRegistro) {

		Preferences id = Preferences.userRoot();

		id.put("idR4", idRegistro);

	}

	public String idObter4() {

		Preferences id = Preferences.userRoot();

		long idRegistro = id.getLong("idR4", 1);

		String idReturn = String.valueOf(idRegistro);

		return idReturn;

	}

	
	
	public String ordenar(String dato) {
		
		String recorrer = dato;
		
	    String[] recorrer2 = recorrer.split("");

	    Arrays.sort(recorrer2);

	    String sorted = "";

	    for(int i =0;i<recorrer2.length;i++){

	      sorted += recorrer2[i];
	    
	    }
		
		return sorted; 
	}
	
	
	

	
	

	// BRB
	public String elementosDiferentes = "Os elementos n�o s�o iguais";
	public String comentariosInativos = "Os coment�rios n�o foram ativados";
	public String correc�oInativa = "A corre��o n�o foi ativada";
	public String semAcesso = "N�o foi possivel aceder ao aplicativo";
	public String semCampoOutput = "O campo output n�o foi adicionado";
	public String comentarioN�oValido = "Os coment�rios n�o foram os ingresados";
	public String crescenteEDecrescente = "Os resultados n�o est�o em uma ordem v�lida";
	public String copiaNaoCriada = "A c�pia n�o foi criada com sucesso";
	public String editado = "O relat�rio n�o foi modificado com sucesso";
	public String eliminar = "O elemento n�o foi deletado";
	public String deletarColuna = "A coluna n�o foi deletada";
	public String exporta��o = "A exporta��o n�o foi realizada com sucesso";
	public String filtros = "Os resultados do filtro n�o s�o iguais";
	public String aplica��oFiltros = "Os filtros n�o foram aplicados com sucesso";
	public String formata��o = "A formata��o n�o foi aplicada";
	public String excluirVariante = "A variante n�o foi excluida";
	public String salvarVariante = "A variante n�o foi salvada com sucesso";
	public String formula = "Os valores n�o s�o iguais";
	public String gruposAvan = "Os resultados n�o s�o iguais";
	public String gruposAvanDif = "Os resultados n�o s�o diferentes";
	public String gruposAvanDel = "Os grupos n�o foram deletados";
	public String ListaSuspensa = "Os coment�rios n�o foram inseridos";
	public String novoRelatorio = "O novo relat�rio n�o foi criado";
	public String colunas = "As colunas n�o foram inseridas";
	public String paginaSeguiente = "N�o se conseguiu avan�ar para a p�gina seguiente";
	public String paginaAnterior = "N�o se conseguiu avan�ar para a p�gina anterior";
	public String paginaFinal = "N�o se conseguiu avan�ar para a �ltima p�gina";
	public String paginaInicial = "N�o se conseguiu avan�ar para a p�gina inicial";
	public String paginaInserida = "N�o foi possiv�l ava�ar para a p�gina inserida";
	public String visualiza�ar = "O modo visualiza��o n�o est� ativado";
	public String compartilharE = "N�o foi possiv�l compartilhar o relat�rio desde o editor";
	public String descompartilharE = "N�o foi possiv�l descompartilhar o relat�rio desde o editor";
	public String compartilharB = "N�o foi possiv�l compartilhar o relat�rio desde a biblioteca";
	public String descompartilharB = "N�o foi possiv�l compartilhar o relat�rio desde a biblioteca";
	public String renomear = "A coluna n�o foi renomeada com sucesso";
	public String reordenar = "A coluna n�o foi reordenada com sucesso";

	// MDR
	public String Editar = "O valor do campo n�o foi editado";
	public String EmpresaVisualizar = "N�o foi possiv�l visalizar o registro";
	public String Filtros = "Os resultados n�o s�o acorde aos filtros";
	public String Criar = "O registro n�o foi criado com sucesso";
	public String Eliminado = "O registro n�o foi eliminado com sucesso";
	public String Detalhes = "As informa��es n�o s�o as esperadas";
	
	public String Atualizar = "N�o foi possivel atualizar os registros";
	
	
	

}
