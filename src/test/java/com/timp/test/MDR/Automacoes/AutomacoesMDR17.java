package com.timp.test.MDR.Automacoes;

import org.openqa.selenium.io.TemporaryFilesystem;
import org.testng.annotations.Test;

import com.sap.timp.base.TestBaseEliel;
import com.sap.timp.base.TestBaseSteven;
import com.timp.test.MDR.AtividadesParaTributacao.AtividadeDeTributacaoXFornecedor.AtividadeDeTributacaoXFornecedorCriar;
import com.timp.test.MDR.AtividadesParaTributacao.AtividadeDeTributacaoXFornecedor.AtividadeDeTributacaoXFornecedorDetalhes;
import com.timp.test.MDR.AtividadesParaTributacao.AtividadeDeTributacaoXFornecedor.AtividadeDeTributacaoXFornecedorEditar;
import com.timp.test.MDR.AtividadesParaTributacao.AtividadeDeTributacaoXFornecedor.AtividadeDeTributacaoXFornecedorExcluir;
import com.timp.test.MDR.AtividadesParaTributacao.AtividadeDeTributacaoXFornecedor.AtividadeDeTributacaoXFornecedorExcluirEmMassa;
import com.timp.test.MDR.AtividadesParaTributacao.AtividadeDeTributacaoXFornecedor.AtividadeDeTributacaoXFornecedorFiltroID;
import com.timp.test.MDR.AtividadesParaTributacao.AtividadeDeTributacaoXFornecedor.AtividadeDeTributacaoXFornecedorVisualizar;
import com.timp.test.MDR.ControleDeCreditoTributario.CodigosDeCredito.CodigosDeCreditoExcluirEmMassa;
import com.timp.test.MDR.ControleDeCreditoTributario.StatusParaBancoIndébitos.StatusParaBancoIndébitosEditar;
import com.timp.test.MDR.ControleDeCreditoTributario.StatusParaBancoIndébitos.StatusParaBancoIndébitosExcluir;
import com.timp.test.MDR.ControleDeCreditoTributario.StatusParaBancoIndébitos.StatusParaBancoIndébitosExcluirMassa;
import com.timp.test.MDR.ControleDeCreditoTributario.StatusParaBancoIndébitos.StatusParaBancoIndébitosVisualizar;
import com.timp.test.MDR.ControleDeCreditoTributario.StatusParaBancoIndébitos.statusParaBancoIndébitosCriar;
import com.timp.test.MDR.ControleDeCreditoTributario.UtilizacaoDosCreditosEmPeriodoAnterior.UtilizacaoDosCreditosEmPeriodoAnteriorExcluirEmMassa;
import com.timp.test.MDR.ParametrosContabilizacao.MapeamentoContabil.MapeamentoContabilExcluirEmMassa;
import com.timp.test.MDR.ParametrosContabilizacao.MapeamentoContabilCorrecao.MapeamentoContabilCorrecaoExcluirEmMassa;
import com.timp.test.MDR.ParametrosContabilizacao.MapeamentoSubstituiçãoContaEstoqueCenáriosCorreções.MapeamentoSubstituicaoContaEstoqueCenariosCorrecoesExcluirEmMassa;
import com.timp.test.MDR.ParametrosOficializacaoLivros.ParametrosOficializacaoLivrosFiltroPorID;
import com.timp.test.MDR.RateioDeConsorcio.RateioDeConsorcioPesquisaPorID;
import com.timp.test.MDR.Siscoserv.RegistroRF.RegistroRFExcluirEmMassa;
import com.timp.test.MDR.Siscoserv.RegistroRF.RegistroRFFiltroPorID;
import com.timp.test.MDR.Siscoserv.RegistroRVS.RegistroRVSExcluirEmMassa;
import com.timp.test.MDR.TabelasApoioESocial.DiagnosticoDeProcessos.DiagnosticoDeProcessosCriar;
import com.timp.test.MDR.TabelasApoioESocial.DiagnosticoDeProcessos.DiagnosticoDeProcessosEditar;
import com.timp.test.MDR.TabelasApoioESocial.DiagnosticoDeProcessos.DiagnosticoDeProcessosExcluir;
import com.timp.test.MDR.TabelasApoioESocial.DiagnosticoDeProcessos.DiagnosticoDeProcessosExcluirEmMassa;
import com.timp.test.MDR.TabelasApoioESocial.DiagnosticoDeProcessos.DiagnosticoDeProcessosVisualizar;
import com.timp.test.MDR.TabelasApoioESocial.Tabela08.ClassificacaoTributariaCriar;
import com.timp.test.MDR.TabelasApoioESocial.Tabela08.ClassificacaoTributariaEditar;
import com.timp.test.MDR.TabelasApoioESocial.Tabela08.ClassificacaoTributariaExcluir;
import com.timp.test.MDR.TabelasApoioESocial.Tabela08.ClassificacaoTributariaExcluirMassa;
import com.timp.test.MDR.TabelasApoioESocial.Tabela08.ClassificacaoTributariaVisualizar;
import com.timp.test.MDR.TabelasApoioESocial.Tabela12.CompatibilidadeEntreLotaçãoExcluirEmMassa;
import com.timp.test.MDR.TabelasApoioESocial.Tabela13.ParteDoCorpoAtingidaExcluirEmMassa;
import com.timp.test.MDR.TabelasApoioESocial.Tabela14.AgenteCausadorDeAcidenteExcluirEmMassa;
import com.timp.test.MDR.TabelasApoioESocial.Tabela16.SGDPCriar;
import com.timp.test.MDR.TabelasApoioESocial.Tabela16.SGDPEditar;
import com.timp.test.MDR.TabelasApoioESocial.Tabela16.SGDPExcluir;
import com.timp.test.MDR.TabelasApoioESocial.Tabela16.SGDPExcluirEmMassa;
import com.timp.test.MDR.TabelasApoioESocial.Tabela16.SGDPVisualizar;
import com.timp.test.MDR.TabelasApoioESocial.Tabela17.DescricaoDeNatDeLesãoExcluirEmMassa;
import com.timp.test.MDR.TabelasApoioESocial.Tabela28.DadosDeRATExcluirEmMassa;
import com.timp.test.MDR.TabelasApoioESocial.Tabela29.CodigoDeTreinamentoESimulacoesDeExerciciosCriar;
import com.timp.test.MDR.TabelasApoioESocial.Tabela29.CodigoDeTreinamentoESimulacoesDeExerciciosEditar;
import com.timp.test.MDR.TabelasApoioESocial.Tabela29.CodigoDeTreinamentoESimulacoesDeExerciciosExcluir;
import com.timp.test.MDR.TabelasApoioESocial.Tabela29.CodigoDeTreinamentoESimulacoesDeExerciciosExcluirEmMassa;
import com.timp.test.MDR.TabelasApoioESocial.Tabela29.CodigoDeTreinamentoESimulacoesDeExerciciosVisualizar;
import com.timp.test.MDR.TabelasApoioSped.CodigoTipoCredito.CodigoTipoCreditoExcluirEmMassa;
import com.timp.test.MDR.TabelasApoioSped.CodigosDasObrigaçoesDeICMSaRecolher.CodigosDasObrigacoesDeICMSaRecolherExcluirEmMassa;
import com.timp.test.MDR.TabelasApoioSped.DeParaLivroApuraçaoXSpedFiscal.DeParaLivroApuracaoXSpedFiscalExcluirEmMassa;
import com.timp.test.MDR.TabelasApoioSped.ObservaçoesdoDocumentoFiscal.ObservaçõesdoDocumentoFiscalExcluirEmMassa;
import com.timp.test.MDR.TabelasApoioSped.TabelaCodigoDaSituaçaoTributaria.TabelaCodigoDaSituaçãoTributáriaExcluirEmMassa;
import com.timp.test.MDR.TabelasApoioSped.TiposDeUtilizacaoDosCreditosFiscais.TiposDeUtilizaçãoDosCreditosFiscaisExcluirEmMassa;
import com.timp.test.MDR.TabelasComplementaresParaObrigaçoesAcessorias.TabelaDeLogradouros.TabelaLogradouroExcluirEmMassa;
import com.timp.test.MDR.TabelasComplementaresParaObrigaçoesAcessorias.TabelaServicos.TabelaServicosExcluirEmMassa;
import com.timp.test.MDR.TaxasDeActualizacao.AliquotaDeTaxaDeActualizacaoPesquisaPorID;
import com.timp.test.MDR.ValorAdicionado.Municipio.MunicipioExcluirEmMassa;

public class AutomacoesMDR17 extends TestBaseSteven{
	
	//Tabela De Apoio E Social > Tabela 17 - Descricao De Nat De Lesão
	DescricaoDeNatDeLesãoExcluirEmMassa descricaoDeNatDeLesãoExcluirEmMassa;

//Tabela De Apoio E Social > Tabela 08 - Classificacao Tributaria
	ClassificacaoTributariaCriar classificacaoTributariaCriar;
	ClassificacaoTributariaEditar classificacaoTributariaEditar;
	ClassificacaoTributariaExcluir classificacaoTributariaExcluir;
	ClassificacaoTributariaVisualizar classificacaoTributariaVisualizar;
	ClassificacaoTributariaExcluirMassa classificacaoTributariaExcluirMassa;
	
//Tabela De Apoio E Social > Tabela 12 - Compatibilidade Entre Lotação

	CompatibilidadeEntreLotaçãoExcluirEmMassa compatibilidadeEntreLotaçãoExcluirEmMassa;
	
//Tabela De Apoio E Social > Tabela 13  -  Parte Do Corpo Atingida
	ParteDoCorpoAtingidaExcluirEmMassa parteDoCorpoAtingidaExcluirEmMassa;
	
//Tabela De Apoio E Social > Tabela 14  - Agente Causador De Acidente 
	AgenteCausadorDeAcidenteExcluirEmMassa agenteCausadorDeAcidenteExcluirEmMassa;
	
//Tabela De Apoio E Social > Tabela 16  - SGDP
	SGDPCriar sgdpCriar;
	SGDPEditar sgdpEditar;
	SGDPExcluir sGDPExcluir;
	SGDPVisualizar sgdpVisualizar;
	SGDPExcluirEmMassa sgdpExcluirEmMassa;

//Rateio De Consorcio
	RateioDeConsorcioPesquisaPorID rateioDeConsorcioPesquisaPorID;

//Parametros Oficializacao Livros
	ParametrosOficializacaoLivrosFiltroPorID parametrosOficializacaoLivrosFiltroPorID;
	
//Parametros Contabilizacao > Mapeamento Substituicao Conta Estoque Cenarios Correcoes
	MapeamentoSubstituicaoContaEstoqueCenariosCorrecoesExcluirEmMassa mapeamentoSubstituicaoContaEstoqueCenariosCorrecoesExcluirEmMassa;
	
//Parametros Contabilizacao > Mapeamento Contabil Correcao
	MapeamentoContabilCorrecaoExcluirEmMassa mapeamentoContabilCorrecaoExcluirEmMassa;
	
//Parametros Contabilizacao > Mapeamento Contabil
	MapeamentoContabilExcluirEmMassa mapeamentoContabilExcluirEmMassa;
	
//Taxas De Actualizacao > Aliquotas De Taxa De Atualizacao
	AliquotaDeTaxaDeActualizacaoPesquisaPorID aliquotasDeTaxaDeActualizacaoPesquisaPorID;

//Controle De Credito Tributario > Utilizacao Dos Creditos Em Periodo Anterior
	UtilizacaoDosCreditosEmPeriodoAnteriorExcluirEmMassa utilizacaoDosCreditosEmPeriodoAnteriorExcluirEmMassa;

//Controle De Credito Tributario > Codigos De Credito
	CodigosDeCreditoExcluirEmMassa codigosDeCreditoExcluirEmMassa;
	
//Siscoserv  Registro RF
	RegistroRFFiltroPorID registroRFFiltroPorID;

//Tabelas Complementares Para Obrigações Acessórias > Tabela Servicos
	TabelaServicosExcluirEmMassa tabelaServicosExcluirEmMassa;

//Tabelas Complementares Para Obrigações Acessórias > Tabela Logradouros
	TabelaLogradouroExcluirEmMassa tabelaLogradouroExcluirEmMassa;
	
//Valor Adicionado > Município
	MunicipioExcluirEmMassa municipioExcluirEmMassa;
	




//Siscoserv  > Registro RVS
	RegistroRVSExcluirEmMassa registroRVSExcluirEmMassa;

//Tabela De Apoio E Social Diagnostico De Processos
	DiagnosticoDeProcessosCriar diagnosticoDeProcessosCriar;
	DiagnosticoDeProcessosEditar diagnosticoDeProcessosEditar;
	DiagnosticoDeProcessosVisualizar diagnosticoDeProcessosVisualizar;
	DiagnosticoDeProcessosExcluir diagnosticoDeProcessosExcluir;
	DiagnosticoDeProcessosExcluirEmMassa diagnosticoDeProcessosExcluirEmMassa;


//Controle de Crédito Tributário > Status para Banco de Indébitos
	statusParaBancoIndébitosCriar StatusParaBancoIndébitosCriar;
	StatusParaBancoIndébitosEditar statusParaBancoIndébitosEditar;
	StatusParaBancoIndébitosExcluirMassa statusParaBancoIndébitosExcluirMassa;
	StatusParaBancoIndébitosVisualizar statusParaBancoIndébitosVisualizar;
	StatusParaBancoIndébitosExcluir statusParaBancoIndébitosExcluir;


//Siscoserv > Registro RF
	RegistroRFExcluirEmMassa registroRFExcluirEmMassa;

//Tabela De Apoio E Social Tabela29 - Codigo De Treinamento E Simulacoes De Exercicios	
	CodigoDeTreinamentoESimulacoesDeExerciciosCriar codigoDeTreinamentoESimulacoesDeExerciciosCriar;
	CodigoDeTreinamentoESimulacoesDeExerciciosEditar codigoDeTreinamentoESimulacoesDeExerciciosEditar;
	CodigoDeTreinamentoESimulacoesDeExerciciosVisualizar codigoDeTreinamentoESimulacoesDeExerciciosVisualizar;
	CodigoDeTreinamentoESimulacoesDeExerciciosExcluirEmMassa codigoDeTreinamentoESimulacoesDeExerciciosExcluirEmMassa;
	CodigoDeTreinamentoESimulacoesDeExerciciosExcluir codigoDeTreinamentoESimulacoesDeExerciciosExcluir;
//Tabela De Apoio E Social Tabela28 - Dados De RAT
	DadosDeRATExcluirEmMassa dadosDeRATExcluirEmMassa;
	
//Tabela Apoio SPED > Tabela Codigo Da Situação Tributária
	TabelaCodigoDaSituaçãoTributáriaExcluirEmMassa tabelaCodigoDaSituaçãoTributáriaExcluirEmMassa;
//Tabela Apoio SPED > Codigo Tipo Credito
	CodigoTipoCreditoExcluirEmMassa codigoTipoCreditoExcluirEmMassa;
	
//Tabela Apoio SPED > Observacoes do Documento Fiscal Registro 0460
	ObservaçõesdoDocumentoFiscalExcluirEmMassa observaçõesdoDocumentoFiscalExcluirEmMassa;

//Tabela Apoio SPED >Tipos De Utilização Dos Creditos Fiscais
	TiposDeUtilizaçãoDosCreditosFiscaisExcluirEmMassa tiposDeUtilizaçãoDosCreditosFiscaisExcluirEmMassa;
	
//TabelaApoioSPED > De Para Livro Apuracao X Sped Fiscal
	DeParaLivroApuracaoXSpedFiscalExcluirEmMassa deParaLivroApuracaoXSpedFiscalExcluirEmMassa;

//Tabela Apoio SPED > Codigos Das Obrigacoes De ICMS a Recolher
	CodigosDasObrigacoesDeICMSaRecolherExcluirEmMassa codigosDasObrigacoesDeICMSaRecolherExcluirEmMassa;

//Atividades Para tributacao >Atividade De Tributacao X Fornecedor	
	AtividadeDeTributacaoXFornecedorCriar atividadeDeTributacaoXFornecedorCriar;
	AtividadeDeTributacaoXFornecedorEditar atividadeDeTributacaoXFornecedorEditar;
	AtividadeDeTributacaoXFornecedorFiltroID atividadeDeTributacaoXFornecedorFiltroID;
	AtividadeDeTributacaoXFornecedorDetalhes atividadeDeTributacaoXFornecedorDetalhes;
	AtividadeDeTributacaoXFornecedorVisualizar atividadeDeTributacaoXFornecedorVisualizar;
	AtividadeDeTributacaoXFornecedorExcluir atividadeDeTributacaoXFornecedorExcluir;
	AtividadeDeTributacaoXFornecedorExcluirEmMassa atividadeDeTributacaoXFornecedorExcluirEmMassa;
	
	
	
	//Tabela De Apoio E Social > Tabela 17 - Descricao De Nat De Lesão
	
	@Test(priority = 0)
	public void descricaoDeNatDeLesãoExcluirEmMassa() {
		System.out.println("-------------------Tabela De Apoio E Social > Tabela 17 - Descricao De Nat De Lesão-------------------------");
		descricaoDeNatDeLesãoExcluirEmMassa = new DescricaoDeNatDeLesãoExcluirEmMassa();
		descricaoDeNatDeLesãoExcluirEmMassa.beforeClass();
		descricaoDeNatDeLesãoExcluirEmMassa.criar();
		descricaoDeNatDeLesãoExcluirEmMassa.afterClass();

	}
//64
//Tabela De Apoio E Social > Tabela 08 - Classificacao Tributaria
	
	@Test(priority = 1)
	public void classificacaoTributariaCriar() {
		System.out.println("-------------------Tabela De Apoio E Social > Tabela 08 - Classificacao Tributaria-------------------------");
		classificacaoTributariaCriar = new ClassificacaoTributariaCriar();
		classificacaoTributariaCriar.beforeClass();
		classificacaoTributariaCriar.criar();
		classificacaoTributariaCriar.afterClass();

	}
	
	
	@Test(priority = 2)
	public void classificacaoTributariaEditar() {
		classificacaoTributariaEditar = new ClassificacaoTributariaEditar();
		classificacaoTributariaEditar.beforeClass();
		classificacaoTributariaEditar.editar();
		classificacaoTributariaEditar.afterClass();

	}
	
	@Test(priority = 3)
	public void classificacaoTributariaVisualizar() {
		classificacaoTributariaVisualizar = new ClassificacaoTributariaVisualizar();
		classificacaoTributariaVisualizar.beforeClass();
		classificacaoTributariaVisualizar.Visualizar();
		classificacaoTributariaVisualizar.afterClass();

	}
	
	@Test(priority = 4)
	public void classificacaoTributariaExcluirMassa() {
		classificacaoTributariaExcluirMassa = new ClassificacaoTributariaExcluirMassa();
		classificacaoTributariaExcluirMassa.beforeClass();
		classificacaoTributariaExcluirMassa.criar();
		classificacaoTributariaExcluirMassa.afterClass();

	}
	
	@Test(priority = 5)
	public void classificacaoTributariaExcluir() {
		classificacaoTributariaExcluir = new ClassificacaoTributariaExcluir();
		classificacaoTributariaExcluir.beforeClass();
		classificacaoTributariaExcluir.excluir();
		classificacaoTributariaExcluir.afterClass();
		
		System.out.println("-------------------Tabela De Apoio E Social > Tabela 08 - Classificacao Tributaria Fim-------------------------");
		

	}
	
	///8
	//Tabela De Apoio E Social > Tabela 12 - Compatibilidade Entre Lotação
	@Test(priority = 8)
	public void compatibilidadeEntreLotaçãoExcluirEmMassa() {
		System.out.println("-------------------Tabela De Apoio E Social > Tabela 08 - Classificacao Tributaria-------------------------");
		compatibilidadeEntreLotaçãoExcluirEmMassa = new CompatibilidadeEntreLotaçãoExcluirEmMassa();
		compatibilidadeEntreLotaçãoExcluirEmMassa.beforeClass();
		compatibilidadeEntreLotaçãoExcluirEmMassa.criar();
		compatibilidadeEntreLotaçãoExcluirEmMassa.afterClass();

	}
	//Tabela De Apoio E Social > Tabela 13  -  Parte Do Corpo Atingida
	@Test(priority = 9)
	public void parteDoCorpoAtingidaExcluirEmMassa() {
		System.out.println("-------------------Tabela De Apoio E Social > Tabela 13  -  Parte Do Corpo Atingida-------------------------");
		parteDoCorpoAtingidaExcluirEmMassa = new ParteDoCorpoAtingidaExcluirEmMassa();
		parteDoCorpoAtingidaExcluirEmMassa.beforeClass();
		parteDoCorpoAtingidaExcluirEmMassa.criar();
		parteDoCorpoAtingidaExcluirEmMassa.afterClass();

	}
	
	//Tabela De Apoio E Social > Tabela 14  - Agente Causador De Acidente 

	@Test(priority = 10)
	public void agenteCausadorDeAcidenteExcluirEmMassa() {
		System.out.println("-------------------Tabela De Apoio E Social > Tabela 14  - Agente Causador De Acidente -------------------------");
		agenteCausadorDeAcidenteExcluirEmMassa = new AgenteCausadorDeAcidenteExcluirEmMassa();
		agenteCausadorDeAcidenteExcluirEmMassa.beforeClass();
		agenteCausadorDeAcidenteExcluirEmMassa.criar();
		agenteCausadorDeAcidenteExcluirEmMassa.afterClass();

	}
	
	
	
//74
//Rateio De Consorcio
	

	@Test(priority = 11)
	public void rateioDeConsorcioPesquisaPorID() {
		System.out.println("-------------------Rateio De Consorcio-------------------------");
		rateioDeConsorcioPesquisaPorID = new RateioDeConsorcioPesquisaPorID();
		rateioDeConsorcioPesquisaPorID.beforeClass();
		rateioDeConsorcioPesquisaPorID.filtro();
		rateioDeConsorcioPesquisaPorID.afterClass();

	}
	
////Parametros Oficializacao Livros
	
	@Test(priority = 12)
	public void parametrosOficializacaoLivrosFiltroPorID() {
		System.out.println("-------------------Parametros Oficializacao Livros-------------------------");
		parametrosOficializacaoLivrosFiltroPorID = new ParametrosOficializacaoLivrosFiltroPorID();
		parametrosOficializacaoLivrosFiltroPorID.beforeClass();
		parametrosOficializacaoLivrosFiltroPorID.filtro();
		parametrosOficializacaoLivrosFiltroPorID.afterClass();
	}
//Parametros Contabilizacao Mapeamento Substituicao Conta Estoque Cenarios Correcoes
	
	@Test(priority = 13)
	public void mapeamentoSubstituicaoContaEstoqueCenariosCorrecoesExcluirEmMassa() {
		System.out.println("-------------------Parametros Contabilizacao Mapeamento Substituicao Conta Estoque Cenarios Correcoes-------------------------");
		mapeamentoSubstituicaoContaEstoqueCenariosCorrecoesExcluirEmMassa = new MapeamentoSubstituicaoContaEstoqueCenariosCorrecoesExcluirEmMassa();
		mapeamentoSubstituicaoContaEstoqueCenariosCorrecoesExcluirEmMassa.beforeClass();
		//mapeamentoSubstituicaoContaEstoqueCenariosCorrecoesExcluirEmMassa.criar();
		mapeamentoSubstituicaoContaEstoqueCenariosCorrecoesExcluirEmMassa.afterClass();
	}
	
	//Parametros Contabilizacao Mapeamento Contabil Correcao
	
	@Test(priority = 14)
	public void mapeamentoContabilCorrecaoExcluirEmMassa() {
		System.out.println("-------------------Parametros Contabilizacao Mapeamento Contabil Correcao-------------------------");
		mapeamentoContabilCorrecaoExcluirEmMassa = new MapeamentoContabilCorrecaoExcluirEmMassa();
		mapeamentoContabilCorrecaoExcluirEmMassa.beforeClass();
		//mapeamentoContabilCorrecaoExcluirEmMassa.criar();
		mapeamentoContabilCorrecaoExcluirEmMassa.afterClass();
	}
	
	//Parametros Contabilizacao Mapeamento Contabil 
	
	@Test(priority = 15)
	public void mapeamentoContabilExcluirEmMassa() {
		System.out.println("-------------------Parametros Contabilizacao Mapeamento Contabil -------------------------");
		mapeamentoContabilExcluirEmMassa = new MapeamentoContabilExcluirEmMassa();
		mapeamentoContabilExcluirEmMassa.beforeClass();
		mapeamentoContabilExcluirEmMassa.criar();
		mapeamentoContabilExcluirEmMassa.afterClass();
	}
	

	//Siscoserv > Registro RF
	
	@Test(priority = 19)
	public void registroRFFiltroPorID() {
		System.out.println("-------------------Siscoserv > Registro RF-------------------------");
		registroRFFiltroPorID = new RegistroRFFiltroPorID();
		registroRFFiltroPorID.beforeClass();
		registroRFFiltroPorID.filtro();
		registroRFFiltroPorID.afterClass();
	}
	
	//Tabelas Complementares Para Obrigações Acessórias > Tabela Servicos
	
	@Test(priority = 20)
	public void tabelaServicosExcluirEmMassa() {
		System.out.println("-------------------Tabelas Complementares Para Obrigações Acessórias > Tabela Servicos-------------------------");
		tabelaServicosExcluirEmMassa = new TabelaServicosExcluirEmMassa();
		tabelaServicosExcluirEmMassa.beforeClass();
		tabelaServicosExcluirEmMassa.criar();
		tabelaServicosExcluirEmMassa.afterClass();
	}
	//Tabelas Complementares Para Obrigações Acessórias > Tabela Logradouros
	
	@Test(priority = 21)
	public void tabelaLogradouroExcluirEmMassa() {
		System.out.println("-------------------Tabelas Complementares Para Obrigações Acessórias > Tabela Logradouros-------------------------");
		tabelaLogradouroExcluirEmMassa = new TabelaLogradouroExcluirEmMassa();
		tabelaLogradouroExcluirEmMassa.beforeClass();
		tabelaLogradouroExcluirEmMassa.criar();
		tabelaLogradouroExcluirEmMassa.afterClass();
	}
	
	//Valor Adicionado > Município
	
	@Test(priority = 22)
	public void municipioExcluirEmMassa() {
		System.out.println("-------------------Valor Adicionado > Município-------------------------");
		municipioExcluirEmMassa = new MunicipioExcluirEmMassa();
		municipioExcluirEmMassa.beforeClass();
		municipioExcluirEmMassa.criar();
		municipioExcluirEmMassa.afterClass();
	}

	//Incentivos Fiscais > Projetos Patrocinados
	//86
	
	
	
	//95

	//Siscoserv  > Registro RVS

	@Test(priority = 24)
	public void registroRVSExcluirEmMassa() {
		System.out.println("-------------------Siscoserv  > Registro RVS-------------------------");
		registroRVSExcluirEmMassa = new RegistroRVSExcluirEmMassa();
		registroRVSExcluirEmMassa.beforeClass();
		registroRVSExcluirEmMassa.criar();
		registroRVSExcluirEmMassa.afterClass();
	}
	//Tabela De Apoio E Social > Diagnostico De Processos
	
	@Test(priority = 25)
	public void diagnosticoDeProcessosCriar() {
		System.out.println("-------------------Tabela De Apoio E Social > Diagnostico De Processos-------------------------");
		diagnosticoDeProcessosCriar = new DiagnosticoDeProcessosCriar();
		diagnosticoDeProcessosCriar.beforeClass();
		diagnosticoDeProcessosCriar.criar();
		diagnosticoDeProcessosCriar.afterClass();
	}
	
	@Test(priority = 26)
	public void diagnosticoDeProcessosEditar() {
		diagnosticoDeProcessosEditar = new DiagnosticoDeProcessosEditar();
		diagnosticoDeProcessosEditar.beforeClass();
		diagnosticoDeProcessosEditar.editar();
		diagnosticoDeProcessosEditar.afterClass();
	}
	

	@Test(priority = 27)
	public void diagnosticoDeProcessosVisualizar() {
		diagnosticoDeProcessosVisualizar = new DiagnosticoDeProcessosVisualizar();
		diagnosticoDeProcessosVisualizar.beforeClass();
		diagnosticoDeProcessosVisualizar.Visualizar();
		diagnosticoDeProcessosVisualizar.afterClass();
	}
	
	@Test(priority = 28)
	public void diagnosticoDeProcessosExcluir() {
		diagnosticoDeProcessosExcluir = new DiagnosticoDeProcessosExcluir();
		diagnosticoDeProcessosExcluir.beforeClass();
		diagnosticoDeProcessosExcluir.excluir();
		diagnosticoDeProcessosExcluir.afterClass();
	}
	
	@Test(priority = 29)
	public void diagnosticoDeProcessosExcluirEmMassa() {
		diagnosticoDeProcessosExcluirEmMassa = new DiagnosticoDeProcessosExcluirEmMassa();
		diagnosticoDeProcessosExcluirEmMassa.beforeClass();
		diagnosticoDeProcessosExcluirEmMassa.criar();
		diagnosticoDeProcessosExcluirEmMassa.afterClass();
		System.out.println("-------------------Tabela De Apoio E Social > Diagnostico De Processos Fim-------------------------");
		
	}
	
	//Siscoserv > Registro RF
	@Test(priority = 30)	
	public void registroRFExcluirEmMassa() {
		System.out.println("-------------------Siscoserv > Registro RF-------------------------");
		registroRFExcluirEmMassa = new RegistroRFExcluirEmMassa();
		registroRFExcluirEmMassa.beforeClass();
		registroRFExcluirEmMassa.criar();
		registroRFExcluirEmMassa.afterClass();
	}
	//Tabela De Apoio E Social Tabela29 - Codigo De Treinamento E Simulacoes De Exercicios	
	
	@Test(priority = 31)	
	public void codigoDeTreinamentoESimulacoesDeExerciciosCriar() {
		System.out.println("-------------------Tabela De Apoio E Social Tabela29 - Codigo De Treinamento E Simulacoes De Exercicios	-------------------------");
		codigoDeTreinamentoESimulacoesDeExerciciosCriar = new CodigoDeTreinamentoESimulacoesDeExerciciosCriar();
		codigoDeTreinamentoESimulacoesDeExerciciosCriar.beforeClass();
		codigoDeTreinamentoESimulacoesDeExerciciosCriar.criar();
		codigoDeTreinamentoESimulacoesDeExerciciosCriar.afterClass();
	}
	
	@Test(priority = 32)	
	public void codigoDeTreinamentoESimulacoesDeExerciciosEditar() {
		codigoDeTreinamentoESimulacoesDeExerciciosEditar = new CodigoDeTreinamentoESimulacoesDeExerciciosEditar();
		codigoDeTreinamentoESimulacoesDeExerciciosEditar.beforeClass();
		codigoDeTreinamentoESimulacoesDeExerciciosEditar.editar();
		codigoDeTreinamentoESimulacoesDeExerciciosEditar.afterClass();
	}
	
	@Test(priority = 33)	
	public void codigoDeTreinamentoESimulacoesDeExerciciosVisualizar() {
		codigoDeTreinamentoESimulacoesDeExerciciosVisualizar= new CodigoDeTreinamentoESimulacoesDeExerciciosVisualizar();
		codigoDeTreinamentoESimulacoesDeExerciciosVisualizar.beforeClass();

		codigoDeTreinamentoESimulacoesDeExerciciosVisualizar.Visualizar();
		codigoDeTreinamentoESimulacoesDeExerciciosVisualizar.afterClass();
	}
	

	@Test(priority = 34)	
	public void codigoDeTreinamentoESimulacoesDeExerciciosExcluir() {
		codigoDeTreinamentoESimulacoesDeExerciciosExcluir= new CodigoDeTreinamentoESimulacoesDeExerciciosExcluir();
		codigoDeTreinamentoESimulacoesDeExerciciosExcluir.beforeClass();
		codigoDeTreinamentoESimulacoesDeExerciciosExcluir.excluir();
		codigoDeTreinamentoESimulacoesDeExerciciosExcluir.afterClass();
	}

	@Test(priority = 35)	
	public void codigoDeTreinamentoESimulacoesDeExerciciosExcluirEmMassa() {
		codigoDeTreinamentoESimulacoesDeExerciciosExcluirEmMassa= new CodigoDeTreinamentoESimulacoesDeExerciciosExcluirEmMassa();
		codigoDeTreinamentoESimulacoesDeExerciciosExcluirEmMassa.beforeClass();
		codigoDeTreinamentoESimulacoesDeExerciciosExcluirEmMassa.criar();
		codigoDeTreinamentoESimulacoesDeExerciciosExcluirEmMassa.afterClass();
		System.out.println("-------------------Tabela De Apoio E Social Tabela29 - Codigo De Treinamento E Simulacoes De Exercicios	Fim-------------------------");
		
	}
	

//Tabela Apoio SPED > Tabela Codigo Da Situação Tributária
	
	@Test(priority = 37)	
	public void tabelaCodigoDaSituaçãoTributáriaExcluirEmMassa() {
		System.out.println("-------------------Tabela Apoio SPED > Tabela Codigo Da Situação Tributária-------------------------");
		tabelaCodigoDaSituaçãoTributáriaExcluirEmMassa = new TabelaCodigoDaSituaçãoTributáriaExcluirEmMassa();
		tabelaCodigoDaSituaçãoTributáriaExcluirEmMassa.beforeClass();
		tabelaCodigoDaSituaçãoTributáriaExcluirEmMassa.criar();
		tabelaCodigoDaSituaçãoTributáriaExcluirEmMassa.afterClass();
	}
	
	//Tabela Apoio SPED > Codigo Tipo Credito
	
	@Test(priority = 38)	
	public void codigoTipoCreditoExcluirEmMassa() {
		System.out.println("-------------------Tabela Apoio SPED > Codigo Tipo Credito-------------------------");
		codigoTipoCreditoExcluirEmMassa = new CodigoTipoCreditoExcluirEmMassa();
		codigoTipoCreditoExcluirEmMassa.beforeClass();
		codigoTipoCreditoExcluirEmMassa.criar();
		codigoTipoCreditoExcluirEmMassa.afterClass();
	}
	
	//Tabela Apoio SPED > Observacoes do Documento Fiscal Registro 0460
	
	@Test(priority = 39)	
	public void observaçõesdoDocumentoFiscalExcluirEmMassa() {
		System.out.println("-------------------Tabela Apoio SPED > Observacoes do Documento Fiscal Registro 0460-------------------------");
		observaçõesdoDocumentoFiscalExcluirEmMassa = new ObservaçõesdoDocumentoFiscalExcluirEmMassa();
		observaçõesdoDocumentoFiscalExcluirEmMassa.beforeClass();
		observaçõesdoDocumentoFiscalExcluirEmMassa.criar();
		observaçõesdoDocumentoFiscalExcluirEmMassa.afterClass();
	}
	
	//Tabela Apoio SPED > Tipos De Utilização Dos Creditos Fiscais
	
	@Test(priority = 40)	
	public void tiposDeUtilizaçãoDosCreditosFiscaisExcluirEmMassa() {
		System.out.println("-------------------Tabela Apoio SPED > Tipos De Utilização Dos Creditos Fiscais-------------------------");
		tiposDeUtilizaçãoDosCreditosFiscaisExcluirEmMassa = new TiposDeUtilizaçãoDosCreditosFiscaisExcluirEmMassa();
		tiposDeUtilizaçãoDosCreditosFiscaisExcluirEmMassa.beforeClass();
		tiposDeUtilizaçãoDosCreditosFiscaisExcluirEmMassa.criar();
		tiposDeUtilizaçãoDosCreditosFiscaisExcluirEmMassa.afterClass();
	}
	
	//TabelaApoioSPED > De Para Livro Apuracao X Sped Fiscal
	
	@Test(priority = 41)	
	public void deParaLivroApuracaoXSpedFiscalExcluirEmMassa() {
		System.out.println("-------------------TabelaApoioSPED > De Para Livro Apuracao X Sped Fiscal-------------------------");
		deParaLivroApuracaoXSpedFiscalExcluirEmMassa = new DeParaLivroApuracaoXSpedFiscalExcluirEmMassa();
		deParaLivroApuracaoXSpedFiscalExcluirEmMassa.beforeClass();
		deParaLivroApuracaoXSpedFiscalExcluirEmMassa.criar();
		deParaLivroApuracaoXSpedFiscalExcluirEmMassa.afterClass();
	}
	//Tabela Apoio SPED > Codigos Das Obrigacoes De ICMS a Recolher
	
	@Test(priority = 42)	
	public void codigosDasObrigacoesDeICMSaRecolherExcluirEmMassa() {
		System.out.println("-------------------Tabela Apoio SPED > Codigos Das Obrigacoes De ICMS a Recolher-------------------------");
		codigosDasObrigacoesDeICMSaRecolherExcluirEmMassa = new CodigosDasObrigacoesDeICMSaRecolherExcluirEmMassa();
		codigosDasObrigacoesDeICMSaRecolherExcluirEmMassa.beforeClass();
		codigosDasObrigacoesDeICMSaRecolherExcluirEmMassa.criar();
		codigosDasObrigacoesDeICMSaRecolherExcluirEmMassa.afterClass();
	}
	
	//Atividades Para tributacao >Atividade De Tributacao X Fornecedor
	
	@Test(priority = 43)	
	public void atividadeDeTributacaoXFornecedorCriar() {
		System.out.println("-------------------Atividades Para tributacao >Atividade De Tributacao X Fornecedor-------------------------");
		atividadeDeTributacaoXFornecedorCriar = new AtividadeDeTributacaoXFornecedorCriar();
		atividadeDeTributacaoXFornecedorCriar.beforeClass();
		atividadeDeTributacaoXFornecedorCriar.criar();
		atividadeDeTributacaoXFornecedorCriar.afterClass();
	}
	
	@Test(priority = 44)	
	public void atividadeDeTributacaoXFornecedorFiltroID() {
		atividadeDeTributacaoXFornecedorFiltroID = new AtividadeDeTributacaoXFornecedorFiltroID();
		atividadeDeTributacaoXFornecedorFiltroID.beforeClass();
		atividadeDeTributacaoXFornecedorFiltroID.filtro();
		atividadeDeTributacaoXFornecedorFiltroID.afterClass();
	}
	
	@Test(priority = 45)	
	public void atividadeDeTributacaoXFornecedorDetalhes() {
		atividadeDeTributacaoXFornecedorDetalhes = new AtividadeDeTributacaoXFornecedorDetalhes();
		atividadeDeTributacaoXFornecedorDetalhes.beforeClass();
		atividadeDeTributacaoXFornecedorDetalhes.Detalhes();
		atividadeDeTributacaoXFornecedorDetalhes.afterClass();
	}
	
	@Test(priority = 46)	
	public void atividadeDeTributacaoXFornecedorVisualizar() {
		atividadeDeTributacaoXFornecedorVisualizar = new AtividadeDeTributacaoXFornecedorVisualizar();
		atividadeDeTributacaoXFornecedorVisualizar.beforeClass();
		atividadeDeTributacaoXFornecedorVisualizar.Visualizar();
		atividadeDeTributacaoXFornecedorVisualizar.afterClass();
	}
	
	@Test(priority = 47)	
	public void atividadeDeTributacaoXFornecedorEditar() {
		atividadeDeTributacaoXFornecedorEditar = new AtividadeDeTributacaoXFornecedorEditar();
		atividadeDeTributacaoXFornecedorEditar.beforeClass();
		atividadeDeTributacaoXFornecedorEditar.editar();
		atividadeDeTributacaoXFornecedorEditar.afterClass();
	}
	
	
	
	
	@Test(priority = 48)	
	public void atividadeDeTributacaoXFornecedorExcluir() {
		atividadeDeTributacaoXFornecedorExcluir = new AtividadeDeTributacaoXFornecedorExcluir();
		atividadeDeTributacaoXFornecedorExcluir.beforeClass();
		atividadeDeTributacaoXFornecedorExcluir.excluir();
		atividadeDeTributacaoXFornecedorExcluir.afterClass();
	}
	
	@Test(priority = 49)	
	public void atividadeDeTributacaoXFornecedorExcluirEmMassa() {
		atividadeDeTributacaoXFornecedorExcluirEmMassa = new AtividadeDeTributacaoXFornecedorExcluirEmMassa();
		atividadeDeTributacaoXFornecedorExcluirEmMassa.beforeClass();
		atividadeDeTributacaoXFornecedorExcluirEmMassa.criar();
		atividadeDeTributacaoXFornecedorExcluirEmMassa.afterClass();
		System.out.println("-------------------Atividades Para tributacao >Atividade De Tributacao X Fornecedor Fim-------------------------");
		
	}
	

	//Tabela De Apoio E Social > Tabela 16  - SGDP
	
	
	
	@Test(priority = 50)
	public void sgdpCriar() {
		System.out.println("-------------------Tabela De Apoio E Social > Tabela 16  - SGDP-------------------------");
		sgdpCriar = new SGDPCriar();
		sgdpCriar.beforeClass();
		sgdpCriar.criar();
		sgdpCriar.afterClass();

	}
	
	
	@Test(priority = 51)
	public void sgdpEditar() {
		sgdpEditar = new SGDPEditar();
		sgdpEditar.beforeClass();
		sgdpEditar.editar();
		sgdpEditar.afterClass();

	}
	
	
	@Test(priority = 52)
	public void sgdpVisualizar() {
		sgdpVisualizar = new SGDPVisualizar();
		sgdpVisualizar.beforeClass();
		sgdpVisualizar.visualizar();
		sgdpVisualizar.afterClass();

	}
	@Test(priority = 53)
	public void sgdpExcluir() {
		sGDPExcluir = new SGDPExcluir();
		sGDPExcluir.beforeClass();
		sGDPExcluir.excluir();
		sGDPExcluir.afterClass();

	}
	
	@Test(priority = 54)
	public void sgdpExcluirEmMassa() {
		
		sgdpExcluirEmMassa = new SGDPExcluirEmMassa();
		sgdpExcluirEmMassa.beforeClass();
		sgdpExcluirEmMassa.criar();
		sgdpExcluirEmMassa.afterClass();
		System.out.println("-------------------Tabela De Apoio E Social > Tabela 16  - SGDP FIN-------------------------");
	}
	
	//Controle de Crédito Tributário > Status para Banco de Indébitos
	//91
	@Test(priority = 55)
	public void statusParaBancoIndébitosCriar() {
		System.out.println("-------------------Controle de Crédito Tributário > Status para Banco de Indébitos-------------------------");
		StatusParaBancoIndébitosCriar = new statusParaBancoIndébitosCriar();
		StatusParaBancoIndébitosCriar.beforeClass();
		StatusParaBancoIndébitosCriar.criar();
		StatusParaBancoIndébitosCriar.afterClass();
	}
	@Test(priority = 56)
	public void StatusParaBancoIndébitosEditar() {
		
		statusParaBancoIndébitosEditar = new StatusParaBancoIndébitosEditar();
		statusParaBancoIndébitosEditar.beforeClass();
		statusParaBancoIndébitosEditar.editar();
		statusParaBancoIndébitosEditar.afterClass();
	}
	@Test(priority = 57)
	public void StatusParaBancoIndébitosVisualizar() {
		
		statusParaBancoIndébitosVisualizar = new StatusParaBancoIndébitosVisualizar();
		statusParaBancoIndébitosVisualizar.beforeClass();
		statusParaBancoIndébitosVisualizar.visualizar();
		statusParaBancoIndébitosVisualizar.afterClass();
	}
	@Test(priority = 58)
	public void StatusParaBancoIndébitosExcluir() {
		
		statusParaBancoIndébitosExcluir = new StatusParaBancoIndébitosExcluir();
		statusParaBancoIndébitosExcluir.beforeClass();
		statusParaBancoIndébitosExcluir.excluir();
		statusParaBancoIndébitosExcluir.afterClass();
	}
	@Test(priority = 59)
	public void StatusParaBancoIndébitosExcluirMassa() {
		
		statusParaBancoIndébitosExcluirMassa = new StatusParaBancoIndébitosExcluirMassa();
		statusParaBancoIndébitosExcluirMassa.beforeClass();
		statusParaBancoIndébitosExcluirMassa.criar();
		statusParaBancoIndébitosExcluirMassa.afterClass();
		System.out.println("-------------------Controle de Crédito Tributário > Status para Banco de Indébitos Fim-------------------------");
	}
	
 
}
