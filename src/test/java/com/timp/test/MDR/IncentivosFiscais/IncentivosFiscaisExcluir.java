package com.timp.test.MDR.IncentivosFiscais;

import org.testng.annotations.Test;

import com.sap.timp.base.TestBaseMassiel;
import com.sap.timp.pageObjectModel.ADM.LoginTC;
import com.sap.timp.pageObjectModel.MDR.AcessarMDRPO;
import com.sap.timp.pageObjectModel.MDR.IncentivosFiscais.IncentivosFiscaisExcluirPO;

import org.testng.annotations.BeforeClass;

import static org.testng.Assert.assertTrue;

import org.testng.annotations.AfterClass;

public class IncentivosFiscaisExcluir extends TestBaseMassiel{

	LoginTC loginTC;
	AcessarMDRPO accesarMDRPO;
	IncentivosFiscaisExcluirPO incentivosFiscaisExcluirPO;

	@BeforeClass
	public void beforeClass() {
		driver = initializationM();
		accesarMDRPO = new AcessarMDRPO();
		loginTC = new LoginTC();
		incentivosFiscaisExcluirPO =new IncentivosFiscaisExcluirPO();

	}

	@AfterClass
	public void afterClass() {
		driver.close();
	}


	@Test()
	public void Excluir() {
		
		loginTC.login();

		accesarMDRPO.acessarMDR();
		
		boolean sucesso =  incentivosFiscaisExcluirPO.Excluir();
		assertTrue(sucesso, Editar);

	}

}
