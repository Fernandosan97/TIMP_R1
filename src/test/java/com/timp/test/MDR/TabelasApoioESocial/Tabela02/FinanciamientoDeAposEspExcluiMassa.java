package com.timp.test.MDR.TabelasApoioESocial.Tabela02;

import org.testng.annotations.Test;

import com.sap.timp.base.TestBaseCristhian;
import com.sap.timp.pageObjectModel.ADM.LoginTC;
import com.sap.timp.pageObjectModel.MDR.AcessarMDRPO;
import com.sap.timp.pageObjectModel.MDR.TabelasApoioESocial.Tabela02.FinanciamientoDeAposEspExcluiMassaPO;
import com.sap.timp.pageObjectModel.MDR.TabelasApoioESocial.Tabela24.CodificaçãoDeAcDeTrabExclusionMassaPO;
import com.sap.timp.pageObjectModel.MDR.TabelasApoioESocial.Tabela25.TiposDeBenefíciosExcluirMassaPO;

import org.testng.annotations.BeforeClass;

import static org.testng.Assert.assertTrue;

import org.testng.annotations.AfterClass;

public class FinanciamientoDeAposEspExcluiMassa extends TestBaseCristhian {

	LoginTC loginTC;
	AcessarMDRPO acessarMDRPO;
	FinanciamientoDeAposEspExcluiMassaPO financiamientoDeAposEspExcluiMassaPO;

	@BeforeClass
	public void beforeClass() {
		driver = initializationC();
		loginTC = new LoginTC();
		acessarMDRPO = new AcessarMDRPO();
		financiamientoDeAposEspExcluiMassaPO = new FinanciamientoDeAposEspExcluiMassaPO();
	}

	@AfterClass
	public void afterClass() {
		driver.close();
	}

	/*
	 * @Test(priority = 0) public void login() { loginTC.login();
	 * 
	 * }
	 * 
	 * @Test(priority = 1) public void acessarMDR() { acessarMDRPO.acessarMDR(); }
	 */
	@Test(priority = 0)
	public void criar() {

		loginTC.login();
		acessarMDRPO.acessarMDR();

		boolean sucesso = financiamientoDeAposEspExcluiMassaPO.criar();
		assertTrue(sucesso, Criar);
	}

	@Test(priority = 1)
	public void excluirMassa() {

		boolean sucesso1 = financiamientoDeAposEspExcluiMassaPO.exluirMassa();
		assertTrue(sucesso1, Eliminado);
	}
}
