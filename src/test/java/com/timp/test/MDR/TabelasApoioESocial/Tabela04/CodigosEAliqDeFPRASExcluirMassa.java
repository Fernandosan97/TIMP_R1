package com.timp.test.MDR.TabelasApoioESocial.Tabela04;

import org.testng.annotations.Test;

import com.sap.timp.base.TestBaseFernando;
import com.sap.timp.pageObjectModel.ADM.LoginTC;
import com.sap.timp.pageObjectModel.MDR.AcessarMDRPO;
import com.sap.timp.pageObjectModel.MDR.TabelasApoioESocial.Tabela04.CodigosEAliqDeFPRASExcluirMassaPO;

import org.testng.annotations.BeforeClass;

import static org.testng.Assert.assertTrue;

import org.testng.annotations.AfterClass;

public class CodigosEAliqDeFPRASExcluirMassa extends TestBaseFernando {
	LoginTC loginTC;
	AcessarMDRPO acessarMDRPO;
	CodigosEAliqDeFPRASExcluirMassaPO codigosEAliqDeFPRASExcluirMassaPO;

	@BeforeClass
	public void beforeClass() {
		driver = initializationF();
		loginTC = new LoginTC();
		acessarMDRPO = new AcessarMDRPO();
		codigosEAliqDeFPRASExcluirMassaPO = new CodigosEAliqDeFPRASExcluirMassaPO();
	}

	@AfterClass
	public void afterClass() {
		driver.close();
	}

	/*
	 * @Test(priority = 0) public void login() { loginTC.login();
	 * 
	 * }
	 * 
	 * @Test(priority = 1) public void acessarMDR() { acessarMDRPO.acessarMDR(); }
	 */
	@Test(priority = 0)
	public void criar() {
		loginTC.login();
		acessarMDRPO.acessarMDR();

		boolean sucesso = codigosEAliqDeFPRASExcluirMassaPO.criar();
		assertTrue(sucesso, Criar);
	}

	@Test(priority = 1)
	public void excluirMassa() {

		boolean sucesso1 = codigosEAliqDeFPRASExcluirMassaPO.exluirMassa();
		assertTrue(sucesso1, Criar);
	}
}
