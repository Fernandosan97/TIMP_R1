package com.timp.test.MDR.TabelasApoioESocial.Tabela05;

import org.testng.annotations.Test;

import com.sap.timp.base.TestBaseFernando;
import com.sap.timp.pageObjectModel.ADM.LoginTC;
import com.sap.timp.pageObjectModel.MDR.AcessarMDRPO;
import com.sap.timp.pageObjectModel.MDR.TabelasApoioESocial.Tabela05.TiposDeRegistroExcluirMassaPO;

import org.testng.annotations.BeforeClass;

import static org.testng.Assert.assertTrue;

import org.testng.annotations.AfterClass;

public class TiposDeRegistroExcluirMassa extends TestBaseFernando {
	LoginTC loginTC;
	AcessarMDRPO acessarMDRPO;
	TiposDeRegistroExcluirMassaPO tiposDeRegistroExcluirMassaPO;

	@BeforeClass
	public void beforeClass() {
		driver = initializationF();
		loginTC = new LoginTC();
		acessarMDRPO = new AcessarMDRPO();
		tiposDeRegistroExcluirMassaPO = new TiposDeRegistroExcluirMassaPO();
	}

	@AfterClass
	public void afterClass() {
		driver.close();
	}

	/*
	 * @Test(priority = 0) public void login() { loginTC.login();
	 * 
	 * }
	 * 
	 * @Test(priority = 1) public void acessarMDR() { acessarMDRPO.acessarMDR(); }
	 */
	@Test(priority = 0)
	public void criar() {
		loginTC.login();
		acessarMDRPO.acessarMDR();
		boolean sucesso = tiposDeRegistroExcluirMassaPO.criar();
		assertTrue(sucesso, Criar);
	}

	@Test(priority = 1)
	public void excluirMassa() {
		boolean sucesso = tiposDeRegistroExcluirMassaPO.exluirMassa();
		assertTrue(sucesso, Criar);
	}
}
