package com.timp.test.TFP.Configurações.Configurações.Período;

import org.testng.annotations.Test;

import com.sap.timp.base.TestBaseKenssy;
import com.sap.timp.pageObjectModel.ADM.LoginTC;
import com.sap.timp.pageObjectModel.TFP.AcessarTFPPO;
import com.sap.timp.pageObjectModel.TFP.Configurações.Configurações.Período.ConfiguraçõesPeríodoCopiarPO;

import org.testng.annotations.BeforeClass;

import static org.testng.Assert.assertTrue;

import java.util.ArrayList;

import org.testng.annotations.AfterClass;

public class ConfiguraçõesPeríodoCopiar extends TestBaseKenssy {
	LoginTC loginTC;
	AcessarTFPPO acessarTFPPO;
	ConfiguraçõesPeríodoCopiarPO configuraçõesPeríodoCopiarPO;

	@BeforeClass
	public void beforeClass() {
		driver = initializationKen();
		loginTC = new LoginTC();
		acessarTFPPO = new AcessarTFPPO();
		configuraçõesPeríodoCopiarPO = new ConfiguraçõesPeríodoCopiarPO();
	}

	@AfterClass
	public void afterClass() {
		driver.close();
	}

	@Test(priority = 1)
	public void copiar() {

		loginTC.login();

		acessarTFPPO.acessarTFP();

		// configuraçõesPeríodoCopiarPO.copiar();
		boolean sucesso = configuraçõesPeríodoCopiarPO.copiar();
		assertTrue(sucesso, Editar);

//		ArrayList<Boolean> sucesso = configuraçõesPeríodoCopiarPO.copiar();
//
//		for (int i = 0; i < sucesso.size(); i++) {
//			assertTrue(sucesso.get(i), visualizaçar);
//		}
	}

}
