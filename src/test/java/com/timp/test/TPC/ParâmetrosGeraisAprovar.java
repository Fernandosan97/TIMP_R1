package com.timp.test.TPC;

import org.testng.annotations.Test;

import com.sap.timp.base.TestBaseCristhian;
import com.sap.timp.pageObjectModel.ADM.LoginTC;
import com.sap.timp.pageObjectModel.TPC.AcessarTPCPO;
import com.sap.timp.pageObjectModel.TPC.Par�metrosGeraisAprovarPO;
import com.sap.timp.pageObjectModel.TPC.Par�metrosGeraisCriarPO;

import org.testng.annotations.BeforeClass;

import static org.testng.Assert.assertTrue;

import org.testng.annotations.AfterClass;

public class Par�metrosGeraisAprovar extends TestBaseCristhian {
	LoginTC loginTC;
	AcessarTPCPO acessarTPCPO;
	Par�metrosGeraisAprovarPO par�metrosGeraisAprovarPO;

	@BeforeClass
	public void beforeClass() {
		driver = initializationC();
		loginTC = new LoginTC();
		acessarTPCPO = new AcessarTPCPO();
		par�metrosGeraisAprovarPO = new Par�metrosGeraisAprovarPO();
	}

	@AfterClass
	public void afterClass() {
		// driver.close();
	}

	@Test(priority = 0)
	public void ingresar() {
		loginTC.login();
	}

	@Test(priority = 1)
	public void ingresarTPC() {

		acessarTPCPO.acessarTPC();

	}

	@Test(priority = 2)
	public void aprovado() {

		boolean sucesso = par�metrosGeraisAprovarPO.aprovado();
	}
}
